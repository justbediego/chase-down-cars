﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WheelAlignment_Script : MonoBehaviour
{
    public List<GameObject> wheelMeshes;
    public List<WheelCollider> wheelColliders;

    void Update()
    {
        for (int i = 0; i < wheelMeshes.Count; i++)
        {
            Vector3 worldPosition;
            Quaternion worldRotation;
            wheelColliders[i].GetWorldPose(out worldPosition, out worldRotation);
            worldRotation = worldRotation * Quaternion.Euler(new Vector3(0, 0, 90));
            wheelMeshes[i].transform.SetPositionAndRotation(worldPosition, worldRotation);
        }
    }
}
