﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PathGenerator : MonoBehaviour
{
    public GameObject cameraObject;
    public List<GameObject> availablePaths;


    //private
    private List<GameObject> generatedPaths = new List<GameObject>();
    private Vector3 currentEndPosition = new Vector3();
    private Vector3 currentBeginPosition = new Vector3();

    public void updateCurrentPositions()
    {
        if (generatedPaths.Count == 0)
        {
            return;
        }
        currentEndPosition = generatedPaths[generatedPaths.Count - 1].GetComponentsInChildren<Transform>().First(c => c.name == "END").transform.position;
        currentBeginPosition = generatedPaths[0].GetComponentsInChildren<Transform>().First(c => c.name == "BEGIN").transform.position;
    }

    private int minDistance = 500;
    private int maxDistance = 2000;
    private System.Random random = new System.Random();

    private void InstantiateRandomPath(bool reverse)
    {
        var path = availablePaths[random.Next(0, availablePaths.Count)];
        var childrenComponents = path.GetComponentsInChildren<Transform>();
        var pathEnd = childrenComponents.First(c => c.name == "END").transform.position;
        var pathBegin = childrenComponents.First(c => c.name == "BEGIN").transform.position;
        var begin = reverse ? pathEnd : pathBegin;
        var end = reverse ? pathBegin : pathEnd;
        var regarding = reverse ? currentBeginPosition : currentEndPosition;
        var newPath = Instantiate(path, regarding - begin, new Quaternion());
        newPath.transform.parent = this.transform;
        if (reverse)
        {
            generatedPaths.Insert(0, newPath);
        }
        else
        {
            generatedPaths.Add(newPath);
        }
        updateCurrentPositions();
    }

    void removePath(bool fromBegin)
    {
        if (generatedPaths.Count == 0)
        {
            return;
        }
        // Debug.Log(fromBegin);
        // Debug.Log("AS FOLLOWS");
        // foreach(var item in generatedPaths){
        //     Debug.Log(item.name);
        // }
        var index = fromBegin ? 0 : generatedPaths.Count - 1;
        var path = generatedPaths[index];
        Destroy(path);
        generatedPaths.RemoveAt(index);
        updateCurrentPositions();
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var endDist = Vector3.Distance(cameraObject.transform.position, currentEndPosition);
        var beginDist = Vector3.Distance(cameraObject.transform.position, currentBeginPosition);
        Debug.Log(endDist + " : " + beginDist);
        if (endDist < minDistance)
        {
            InstantiateRandomPath(false);
        }
        if (beginDist < minDistance)
        {
            //InstantiateRandomPath(true);
        }
        if (endDist > maxDistance)
        {
            removePath(false);
        }
        if (beginDist > maxDistance)
        {
            removePath(true);
        }
    }
}
