﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DriverEngine : MonoBehaviour
{
    public GameObject rearCamera;
    public GameObject vehicle;
    private Vector3 cameraPosition = new Vector3(0, 0, 0);
    private Random random = new Random();

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame

    void ShakeCamera()
    {
        transform.position = vehicle.transform.position;
        var rotation = vehicle.transform.eulerAngles;
        rotation.z = 0;
        rotation.x = 0;
        if (Random.Range(0.0f, 1000.0f) < 100)
        {
            cameraPosition.x = Random.Range(-.2f, .2f);
            cameraPosition.z = Random.Range(-.3f, .3f);
            cameraPosition.y = Random.Range(-.4f, .4f);
        }
        var currentCameraPosition = rearCamera.transform.localPosition;
        var currentCameraRotation = transform.eulerAngles;
        currentCameraPosition += (cameraPosition - currentCameraPosition) / (Time.deltaTime * 100000);
        var yRotation = (rotation.y - currentCameraRotation.y) % 360;
        if (yRotation > 180)
        {
            yRotation = -360 + yRotation;
        }
        else if (yRotation < -180)
        {
            yRotation = 360 + yRotation;
        }
        currentCameraRotation.Set(0, currentCameraRotation.y + yRotation / (Time.deltaTime * 50000), 0);
        rearCamera.transform.localPosition = currentCameraPosition;
        transform.eulerAngles = currentCameraRotation;
        //Debug.Log(rotation.y + " -> " + currentCameraRotation.y + " -> " + yRotation);
    }

    void Update()
    {
        ShakeCamera();
        List<WheelCollider> wheels = vehicle.GetComponentsInChildren<WheelCollider>().ToList();
        Rigidbody vehicleRigidBody = vehicle.GetComponent<Rigidbody>();
        float motorTorque = 0;
        wheels.Where(w => w.name.Contains("Front")).ToList().ForEach(w =>
        {
            motorTorque = w.motorTorque;
        });
        if (Input.GetKey(KeyCode.W))
        {
            motorTorque = -.5f * 5000 * Time.deltaTime * 50.0f;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            motorTorque = .5f * 5000 * Time.deltaTime * 50.0f;
        }
        wheels.Where(w => w.name.Contains("Rear")).ToList().ForEach(w =>
        {
            w.motorTorque = motorTorque;
        });

        float steerAngle = 0;
        if (Input.GetKey(KeyCode.A))
        {
            steerAngle = -45;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            steerAngle = 45;
        }
        wheels.Where(w => w.name.Contains("Front")).ToList().ForEach(w =>
        {
            w.steerAngle = steerAngle;
        });
    }
}
